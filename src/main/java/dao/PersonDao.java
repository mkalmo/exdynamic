package dao;

import java.util.*;

import javax.persistence.*;

import model.Person;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PersonDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Person person) {
        if (person.getId() == null) {
            em.persist(person);
        } else {
            em.merge(person);
        }
    }

    public List<Person> getAllPersons() {
        return em.createQuery("select p from Person p",
                Person.class).getResultList();
    }

    public Person getPersonById(Long personId) {
        return em.find(Person.class, personId);
    }
}
