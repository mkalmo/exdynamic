import model.Person;
import model.Phone;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import conf.DataConfig;
import dao.PersonDao;

public class Main {

    public static void main(String[] args) {

        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(DataConfig.class);

        PersonDao dao = ctx.getBean(PersonDao.class);

        System.out.println(dao.getAllPersons());

        ((ConfigurableApplicationContext) ctx).close();
    }

}
