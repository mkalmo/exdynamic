package controller;

import java.util.List;

import javax.annotation.Resource;

import model.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import view.PersonForm;
import dao.PersonDao;

@Controller
public class PersonController {

    @Resource
    private PersonDao personDao;

    @RequestMapping("/")
    public String personList(ModelMap model) {
        return "list";
    }

    @RequestMapping("/add")
    public String showForm(@ModelAttribute("personForm") PersonForm form) {
        return "form";
    }

    @RequestMapping("/view/{personId}")
    public String view(
            @ModelAttribute("personForm") PersonForm form,
            @PathVariable Long personId) {

        return "form";
    }

    @RequestMapping("/save")
    public String saveForm(
            @ModelAttribute("personForm") PersonForm form) {

        return "redirect:/";
    }
}