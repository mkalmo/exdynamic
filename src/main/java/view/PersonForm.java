package view;

import model.*;

public class PersonForm {

    private Person person;

    private String saveButton;

    private String addPhoneButton;

    private boolean disabled = false;

    public String getSaveButton() {
        return saveButton;
    }
    public void setSaveButton(String saveButton) {
        this.saveButton = saveButton;
    }
    public String getAddPhoneButton() {
        return addPhoneButton;
    }
    public void setAddPhoneButton(String addPhoneButton) {
        this.addPhoneButton = addPhoneButton;
    }
    public Person getPerson() {
        return person;
    }
    public void setPerson(Person person) {
        this.person = person;
    }
    public boolean isDisabled() {
        return disabled;
    }
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
