package model;

import javax.persistence.*;

@Entity
public class Phone {

    @Id
    @GeneratedValue
    private Long id;

    private String number;

    @Transient
    private String deleteButton;

    public Phone(String number) {
        this.number = number;
    }

    public Phone(Long id) {
        this.id = id;
    }

    public Phone() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(String deleteButton) {
        this.deleteButton = deleteButton;
    }

    @Override
    public String toString() {
        return id + " - " + number;
    }

}
