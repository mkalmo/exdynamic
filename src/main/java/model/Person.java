package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private List<Phone> phones = new ArrayList<Phone>();

    public Person() {}

    public Person(String name) {
        this.name = name;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<Phone> getPhones() {
        return phones;
    }
    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }
    public void addPhone(Phone phone) {
        phones.add(phone);
    }
    public Phone getPhoneWithDeletePressed() {
        for (Phone phone : phones) {
            if (phone.getDeleteButton() != null) {
                return phone;
            }
        }

        return null;
    }
    public void removePhone(Phone phone) {
        phones.remove(phone);
    }
    @Override
    public String toString() {
        return "Person [id=" + id +
               ", name=" + name +
               ", phones=" + phones + "]";
    }

}
